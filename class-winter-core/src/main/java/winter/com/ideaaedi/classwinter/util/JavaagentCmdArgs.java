package winter.com.ideaaedi.classwinter.util;

import winter.com.ideaaedi.classwinter.author.JustryDeng;
import winter.com.ideaaedi.classwinter.exception.ClassWinterException;

/**
 * 在使用class-winter进行解密时，如果不需要输入参数，那么可以直接java -javaagent:xxx.jar -jar xxx.jar
 * 如果需要传参，下面两种方式都可以
 *     java -javaagent:xxx.jar="k1=v1,k2=v2" -jar xxx.jar
 *     java -javaagent:xxx.jar=k1=v1,k2=v2 -jar xxx.jar
 *
 *  注: 这里k...只支持本类中的属性password和debug。
 *
 * @author {@link JustryDeng}
 * @since 2021/6/1 22:15:28
 */
public class JavaagentCmdArgs {
    
    private String password;
    
    private boolean debug;
    
    public String getPassword() {
        return password;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }
    
    public boolean isDebug() {
        return debug;
    }
    
    public void setDebug(boolean debug) {
        this.debug = debug;
    }
    
    @Override
    public String toString() {
        return "JavaagentCmdArgs{" +
                "password='" + password + '\'' +
                ", debug=" + debug +
                '}';
    }
    
    /**
     * 解析java -javaagent:xxx.jar="k1=v1,k2=v2" -jar xxx.jar中的k1=v1,k2=v2字符串成JavaagentCmdArgs对象
     *
     * <p>
     *  注：根据本方法的逻辑，如果key重复，那么后面的value值会覆盖前面的。
     *  注：根据本方法的逻辑，如果debug的值不为true,那么其值就为false。
     *
     * @param args
     *            使用javaagent代理时，输入的参数， 如: k1=v1,k2=v2
     * @return 解析出来的对象（这个对象本身一定不为null）
     */
    public static JavaagentCmdArgs parseJavaagentCmdArgs(String args) {
        if (args == null) {
            return new JavaagentCmdArgs();
        }
        if (args.contains(Constant.WHITE_SPACE)) {
            throw new ClassWinterException("-javaagent args [" + args + "] cannot contain whitespace.");
        }
        // args形如 k1=v1,k2=v2
        String[] keyValueArr = args.split(",");
        JavaagentCmdArgs instance = new JavaagentCmdArgs();
        for (String keyValue : keyValueArr) {
            keyValue = keyValue.trim();
            if (keyValue.startsWith("password=")) {
                instance.setPassword(keyValue.substring("password=".length()));
                continue;
            }
            if (keyValue.startsWith("debug=")) {
                instance.setDebug(Boolean.parseBoolean(keyValue.substring("debug=".length())));
            }
        }
        return instance;
    }
}
