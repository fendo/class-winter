package winter.com.ideaaedi.classwinter.util;

import winter.com.ideaaedi.classwinter.author.JustryDeng;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * 常量类
 *
 * @author {@link JustryDeng}
 * @since 2021/4/23 0:36:14
 */
public interface Constant {
    
    /**
     * 被混淆了的内容的提示信息
     */
    StringBuffer TIPS = new StringBuffer("ERROR !!!!!!!!!!! Jar(or War) has been protected by class-winter. Please use javaagent re-start project. !!!!!!!!!!!");
    
    
    /**
     * 印章， 若class中存在此印章内容，也能说明该class被class-winter加密过
     */
    String SEAL = "At " + LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME).replace("T"," ") + "\t" + "Random-Character " + new String(EncryptUtil.generateCharArr(32));
    
    /**
     * 默认需要加密的xml一级节点名称
     */
    String DEFAULT_XML_NODE_NAMES = "resultMap,sql,insert,update,delete,select";
    
    /**
     * 解压jar/war包时，临时目录后缀名
     */
    String TMP_DIR_SUFFIX = "__temp__";
    
    /**
     * lib
     */
    String LIB = "lib";
    
    /**
     * classes
     */
    String CLASSES = "classes";
    
    /**
     * 默认的class-winter加密存储目录base
     */
    String DEFAULT_ENCRYPTED_BASE_SAVE_DIR = "META-INF/winter/";
    
    /**
     * 默认的class-winter加密classes相关信息的存储目录
     */
    String DEFAULT_ENCRYPTED_CLASSES_SAVE_DIR = "META-INF/winter/classes/";
    
    /**
     * 默认的class-winter加密其它文件(如.yml .yaml .xml .properties)相关信息的存储目录
     */
    String DEFAULT_ENCRYPTED_NON_CLASSES_SAVE_DIR = "META-INF/winter/non-classes/";
    
    /**
     * 记录已加密.class的全类名的清单文件
     */
    String ALREADY_ENCRYPTED_CLASS_CHECKLIST_CLASSES_SAVE_FILE = "META-INF/winter/checklist.classes.winter";
    
    /**
     * 记录已加密的非class文件的条目名清单文件(内容形如: BOOT-INF/classes/application.properties,BOOT-INF/classes/application-dev.properties)
     */
    String ALREADY_ENCRYPTED_NON_CLASS_FILE_CHECKLIST_SAVE_FILE = "META-INF/winter/checklist.non-classes.winter";
    
    /**
     * 记录已加密.class的全类名的清单文件
     */
    String CHECKLIST_CLASS_FILE_SIMPLE_NAME = "checklist.classes.winter";
    
    /**
     * 记录本次加密印章的文件
     */
    String SEAL_FILE = "META-INF/winter/seal.winter";
    
    /**
     * 记录本次加密印章的文件
     */
    String SEAL_FILE_SIMPLE_NAME = "seal.winter";
    
    /**
     * pom.xml文件所在的祖辈目录
     */
    String POM_XML_ROOT = "META-INF/maven";
    
    /**
     * 汇总记录项目中那些本身就已经被class-winter混淆了的lib的checklist
     */
    String CHECKLIST_OF_ALL_LIBS = DEFAULT_ENCRYPTED_CLASSES_SAVE_DIR + "checklist-of-all-libs.winter";
    
    /**
     * 当用户不主动指定密码时，class_winter会自动生成加密密码，并存至此处
     */
    String PWD_WINTER = "META-INF/winter/pwd.winter";
    
    /**
     * 当用户不主动指定密码时，class_winter会自动生成加密密码，并存至此处
     */
    String PWD_WINTER_SIMPLE_NAME = "pwd.winter";
    
    /**
     * 记录加密时，用户是否输入了密码
     */
    String USER_IF_INPUT_PWD = "META-INF/winter/userIfInputPwd.winter";
    
    /**
     * 记录加密时，用户是否输入了密码
     */
    String USER_IF_INPUT_PWD_SIMPLE_NAME = "userIfInputPwd.winter";
    
    /**
     * BOOT-INF
     */
    String BOOT_INF = "BOOT-INF";
    
    /**
     * WEB-INF
     */
    String WEB_INF = "WEB-INF";
    
    /**
     * class文件后缀
     */
    String CLASS_SUFFIX = ".class";
    
    /**
     * jar包后缀
     */
    String JAR_SUFFIX = ".jar";
    
    /**
     * xml文件后缀
     */
    String XML_SUFFIX = ".xml";
    
    /**
     * war包后缀
     */
    String WAR_SUFFIX = ".war";
    
    /**
     * jar协议
     */
    String JAR_PROTOCOL = "jar:";
    
    /**
     * war协议
     */
    String WAR_PROTOCOL = "war:";
    
    /**
     * file协议
     */
    String FILE_PROTOCOL = "file:";
    
    /**
     * 文件路径分隔符
     */
    String LINUX_FILE_SEPARATOR = "/";
    
    /**
     * /classes/
     */
    String CLASSES_DIR = "/classes/";
    
    /**
     * 逗号
     */
    String COMMA = ",";
    
    /**
     * jar包中文件URL有专用格式 jar:!/{jar-entry}
     */
    String JAR_FILE_URL_SPECIAL_SIGN = "!";
    
    /**
     * 换行符
     */
    String LINE_SEPARATOR = "\r\n";
    
    /**
     * 空格
     */
    String WHITE_SPACE = " ";
    
    /**
     * 10
     */
    int TEN = 10;
    
    /**
     * groupId
     */
    String GROUP_ID = "com.idea-aedi";
    
    /**
     * artifactId
     */
    String ARTIFACT_ID = "class-winter-maven-plugin";
    
    /**
     * PREMAIN_CLASS
     */
    String PREMAIN_CLASS = "Premain-Class: ";
}
